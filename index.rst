.. |label| replace:: Anzeige Filter als Range
.. |snippet| replace:: FvFilterRange
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.4.0
.. |maxVersion| replace:: 5.5.x
.. |Version| replace:: 1.0.0
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis


Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |Version|

Beschreibung
------------
In den "Artikel-Eigenschaften" im Backend kann für jeden Filter definiert werden ob dies als Range Slider angezeigt werden soll.

Frontend
--------
Die konfigurierten Filter werden im Listing als Range Slider angezeigt.

.. image:: frontend1.png


Backend
-------
In den "Artikel-Eigenschaften" beim Editieren einer Gruppe wird eine zusätzliche Maske eingeblendet.

Folgendes kann in der Eingabemaske konfiguriert werden:

- "Range Slider aktivieren": ja/nein
- "Range Slider Einheit": Einheit (wird als Textbaustein angelegt)
- "Anzahl Nachkommastellen": Nachkommastellen bei der Anzeige im Frontend (default 1)

.. image:: backend1.png


technische Beschreibung
------------------------

Plugin-Einstellungen:
_____________________
Keine Einstellungsmöglichkeit vorhanden.


Shop-Datenbank:
_______________
Keine Zusatztabelle vorhanden.


Modifizierte Template-Dateien
-----------------------------
Keine Template-Dateien wurde modifiziert.


